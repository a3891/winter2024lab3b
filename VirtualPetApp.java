import java.util.Scanner;
public class VirtualPetApp {
	public static void main (String[] args) {
		
		//Cat cat1 = new Cat();
		//cat1.name = "Peach";
		//cat1.color = "Orange";
		//cat1.age = 3;
		
		//System.out.println(cat1.presentCat());
		//cat1.catAge();
		
		//Part 2
		Cat[] clowder = new Cat[4];
		//System.out.println(clowder.length);
		for (int i = 0; i < clowder.length; i++) {
			Scanner scan = new Scanner(System.in);
			clowder[i] = new Cat();
			System.out.println("What's the name of the cat?");
			clowder[i].name = scan.nextLine();
			System.out.println("What's the color of the cat?");
			clowder[i].color = scan.nextLine();
			System.out.println("How old is your cat?");
			clowder[i].age = scan.nextInt();
		}
		System.out.println("Info about your last cat: " + clowder[3].name + ", " + clowder[3].color + ", " + clowder[3].age + ".");
		System.out.println(clowder[0].presentCat());
		clowder[0].catAge();
	}
	
}