public class Cat {
	
	public String name;
	public String color;
	public int age;
	
	public String presentCat() {
		return name + ", the " + color + " cat, says 'Meow!'";
	}
	
	public void catAge() {
		if (age == 0) {
			System.out.println("In human years, your cat still was just born!");
		}
		else if (age == 1) {
			System.out.println("Your cat is 7 years old!");
		}
		else if (age == 2) {
			System.out.println("Your cat is 15 years old!");
		}
		else if (age == 3) {
			System.out.println("Your cat is 28 years old!");
		}
		//After this, for every human year the cat ages 4 cat years
		else if (age > 3) {
			int new_age = ((age - 3) * 4) + 28; //example: 4 years = 32 cat years, 5 years = 36 cat years, and so on
			System.out.println("Your cat, " + name + ", is " + new_age + " years old!");
		}
	}
}